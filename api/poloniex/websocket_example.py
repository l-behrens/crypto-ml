#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import websockets
import time
import json
import logging
from api.poloniex.pair_ids import channel_ids

__author__ = 'Lars Behrens'
__copyright__ = 'Copyright 2021, crypto-ml'
__credits__ = []
__license__ = 'private'
__version__ = '{mayor}.{minor}.{rel}'
__maintainer__ = 'Lars Behrens'
__email__ = 'behrens_lars@gmx.de'
__status__ = 'develop'


class PoloniexCrawler(object):
    """Description:
    * keeps up poloniex websocket connections
    * manages poloniex websocket channel subscriptions
    * writes channel responses into a FIFO Queue

    # TODO:
    # Graceful exit
    """

    def __init__(self, fifo, data, uri):
        self._queue = fifo
        self._data = data
        self._uri = uri

    async def run(self):
        await self.run_websocket(self._uri, self._data)

    @staticmethod
    def get_nonce():
        """generates nonces"""
        return str(int(time.time() * 1000))

    @staticmethod
    async def subscribe_channel(websocket, channel):

        logging.log(logging.INFO, f"connecting to channels: {channel}")
        await websocket.send(json.dumps({
            "command": "subscribe",
            "channel": f"{channel}"
        }))
        logging.log(logging.INFO, f"connected to channel: {channel}")

    async def run_websocket(self, uri, data):
        """Connect to websocket @uri and subscribe to channels: @channels"""
        async with websockets.connect(uri) as websocket:
            logging.log(logging.INFO, f"connected to websocket: {uri}")
            for channel in data:
                await self.subscribe_channel(websocket, channel)

            async for message in websocket:
                await self._queue.put(message)

        logging.log(logging.INFO, f"disconnected from websocket: {uri}")


class PoloniexDispatcher(object):
    """Description:
    * fetches Poloniex data structures from a Queue
    * filters fetched data structures
    * dispatches fetched data to data processors
    * data processors transform data structures and write them into a message broker

    # TODO:
    # Implement filters
    # Implement data processors
    # Implement message broker connection
   """

    def __init__(self, fifo):
        self._queue = fifo

    async def run(self):

        while True:
            await self.dispatch_poloniex_message(
                await self.prepare_poloniex_messages(
                    await self._queue.get()
                )
            )

    @staticmethod
    async def prepare_poloniex_messages(message):

        try:
            decoded_msg = json.loads(message)
        except KeyError as e:
            logging.log(logging.WARNING, f"sick response: {message}, stack: {e}")
        else:
            return decoded_msg

    async def dispatch_poloniex_message(self, message):
        """dispatches poloniex websocket messages"""
        channel = int(message[0]) if message else None

        if channel < 1000:
            await self.process_poloniex_price_aggregated_book(message)
        elif channel == 1002:
            await self.process_poloniex_ticker_data(message)
        elif channel == 1003:
            await self.process_poloniex_24_hour_exchange_volume(message)
        elif channel == 1010:
            await self.process_poloniex_heartbeat(message)
        elif channel is None:
            logging.log(logging.INFO, f"empty message")
        else:
            logging.log(logging.WARNING, f"unknown channel: {channel}")

    @staticmethod
    async def process_poloniex_ticker_data(data):

        print(data)

    @staticmethod
    async def process_poloniex_24_hour_exchange_volume(data):

        print(data)

    @staticmethod
    async def process_poloniex_price_aggregated_book(data):

        print(data)

    @staticmethod
    async def process_poloniex_heartbeat(data):

        print(data)


class Helpers(object):

    @staticmethod
    def build_channels(scope):

        return [
            item[0] for item in channel_ids.items() if item[1]["scope"] in scope
        ]


def run():

    logging.getLogger().setLevel(logging.INFO)
    channels = Helpers.build_channels(["public"])

    q = asyncio.Queue()
    poloniexcrawler = PoloniexCrawler(q, channels, uri='wss://api2.poloniex.com')
    dispatcher = PoloniexDispatcher(q)

    asyncio.get_event_loop().create_task(poloniexcrawler.run())
    asyncio.get_event_loop().create_task(dispatcher.run())
    asyncio.get_event_loop().run_forever()


if __name__ == "__main__":

    run()
